from django.db import models
# from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    image = models.URLField()
    location = models.CharField(max_length=200)

    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.PROTECT,
        null=True,
    )

    def __str__(self):
        return self.style_name

    class Meta:
        ordering = ("style_name",)
