import { NavLink, useLocation } from 'react-router-dom';

function Nav() {
  const location = useLocation();

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Wardrobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" style={{paddingTop: '12px'}} aria-current="page" to="/">Home</NavLink>
            </li>
            <ul className="navbar-nav">
              {location.pathname === '/hats/list' && (
                <>
                  <li className="nav-item">
                    <NavLink className="nav-link" style={{paddingTop: '12px'}} id='shoes' to="/shoes/list">Shoes</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active btn btn-primary btn-lg bg-white btn-outline-info" style={{paddingRight: '10px'}} id='hats' to="/hats/list">Create</NavLink>
                  </li>
                </>
              ) || location.pathname === '/' && (
                <li className="nav-item">
                  <NavLink className="nav-link" style={{paddingTop: '12px'}} id='hats' to="/hats/list">Hats</NavLink>
                </li>
              )}
              {location.pathname === '/shoes/list' && (
                <>
                  <li className="nav-item">
                    <NavLink className="nav-link" style={{paddingTop: '12px'}} id='hats' to="/hats/list">Hats</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active btn btn-primary btn-lg bg-white btn-outline-info" style={{paddingRight: '10px'}} id='shoes' to="/shoes/new">Create</NavLink>
                  </li>
                </>
              ) || location.pathname === '/' && (
                <li className="nav-item">
                  <NavLink className="nav-link" style={{paddingTop: '12px'}} id='shoes' to="/shoes/list">Shoes</NavLink>
                </li>
              )}
              {location.pathname === '/shoes/new' && (
                <>
                <li className="nav-item">
                  <NavLink className="nav-link" style={{paddingTop: '12px'}} id='hats' to="/hats/list">Hats</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" style={{paddingTop: '12px'}} id='shoes' to="/shoes/list">Shoes</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link active btn btn-primary btn-lg bg-white btn-outline-info" to="/hats/new">Add to hat collection</NavLink>
                </li>
                </>
              )}
              {location.pathname === '/hats/new' && (
                <>
                <li className="nav-item">
                  <NavLink className="nav-link" style={{paddingTop: '12px'}} id='hats' to="/hats/list">Hats</NavLink>
                </li>
                <li className="nav-item">
                <NavLink className="nav-link" style={{paddingTop: '12px'}} id='shoes' to="/shoes/list">Shoes</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link active btn btn-primary btn-lg bg-white btn-outline-info" to="/shoes/new">Add to shoe collection</NavLink>
                </li>
                </>
              )}
            </ul>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
