import React, { useState, useEffect } from 'react';

function HatsForm () {
    const [fabric, setFabric] = useState('');
    const [style_name, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [image, setImage] = useState('');
    const [locations, setLocations] = useState([]);
    const [selectedLocations, setSelectedLocations] = useState('');

    const updateInputState = function ({ target }, cb) {
      const { value } = target;

      cb(value);
    };

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {
        fabric,
        style_name,
        color,
        image,
        location:selectedLocations,
  };

    const hatsUrl = 'https://localhost:8090/api/locations/hats/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

  try {
      const response = await fetch(hatsUrl, fetchConfig);
      if (response.ok) {
        const newHat = await response.json();
        console.log("Hat created:", newHat);
        event.target.reset()
      } else {
        throw new Error("Network response was not ok");
      }
    } catch (error) {
      console.error("Error adding location:", error);
    }
  };

  const fetchLocations = async () => {
      const locationsUrl = 'http://localhost:8100/api/locations/';
      const response = await fetch(locationsUrl);
      if (response.ok) {
        const data = await response.json();
        const sortedLocations = data.locations.sort((a, b) => a - b);
        setLocations(sortedLocations);
    } else {
        console.error("Error fetching locations:", response.statusText);
    }
};

        useEffect(()=> {
        fetchLocations();
    }, []);

    return (
      <>
          <div className="row">
          <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
              <h1>Add a new hat to your collection</h1>
              <form id="create-hat-form" onSubmit={handleSubmit}>
                  <div className="form-floating mb-3">
                  <input
                      placeholder=""
                      required
                      type="text"
                      name="style_name"
                      id="style_name"
                      className="form-control"
                      onChange={(event) => updateInputState(event, setStyleName)}
                  />
                  <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input
                      placeholder=""
                      required
                      type="text"
                      name="fabric"
                      id="fabric"
                      className="form-control"
                      onChange={(event) => updateInputState(event, setFabric)}
                  />
                  <label htmlFor="model_name">Style Name</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input
                      placeholder=""
                      required
                      type="text"
                      name="color"
                      id="color"
                      className="form-control"
                      onChange={(event) => updateInputState(event, setColor)}
                  />
                  <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input
                      placeholder=""
                      required
                      type="url"
                      name="image"
                      id="image"
                      className="form-control"
                      onChange={(event) => updateInputState(event, setImage)}
                  />
                  <label htmlFor="color">Hat Image URL</label>
                  </div>
                  <div className="mb-3">
                      <select
                          required
                          name="location"
                          id="location"
                          className="form-select"
                          value={selectedLocations}
                          onChange={(event) => setSelectedLocations(event.target.value)}
                          >
                          <option value="">Choose a location</option>
                          {locations.map((location) => (
                          <option key={location.id} value={location.id}>
                              {location.location_number}
                          </option>
                          ))}
                      </select>
                  </div>

                  <button className="btn btn-primary">Create</button>
              </form>
              </div>
          </div>
          </div>
      </>
  );
  }




export default HatsForm;
