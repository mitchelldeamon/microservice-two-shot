import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';


function ShoesCard(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const shoe = data;
                console.log(data);
                return (
                <div key={shoe.id} className="card mb-5 shadow rounded me-4">
                    <div className="d-flex justify-content-evenly card-title text-center mt-3">
                        <h4 className="card-text mb-2">{shoe.manufacturer}: {shoe.model_name}</h4>
                    </div>
                    <img src={shoe.picture || 'https://via.placeholder.com/300'} className="card-img-top" />
                    <div className="card">
                        <ul className="card-list list-group-flush">
                            <li className="list-group-item" style={{paddingRight: '60px'}}>{shoe.color}</li>
                            <li className="list-group-item" style={{paddingRight: '50px'}}>Bin Number {shoe.bin.bin_number}</li>
                        </ul>
                    </div>
                    <div className="card-body">
                        <button type="button" className="btn btn-outline-danger " onClick={() => props.onDelete(shoe.id)}> Delete</button>
                    </div>

                </div>
                );
            })}
        </div>
    );
}
// console.log("start shoe list")
function ShoesList() {
    const [shoeCards, setShoeCards] = useState([[], [], []]);
    console.log("start fetch data")
    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";
        console.log("start try")
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                // console.log(data);
                const columns = [[], [], []];
                let i = 0;
                for (let shoe of data.shoes) {

                    columns[i].push(shoe)
                    i = i + 1;
                    if (i > 2) {
                        i = 0;
                    }
                }
                setShoeCards(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    const onDelete = async (shoeID) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${shoeID}/`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (response.status >= 200 && response.status < 300) {
            fetchData();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="py-5 text-center">
            <h2 className="display-5 fw-bold mb-5">Shoe Collection</h2>
            <div className="row">
                {shoeCards.map((shoesList, index) => {
                    return (
                        <ShoesCard key={index} list={shoesList} onDelete={onDelete}/>
                    );
                })}
            </div>
        </div>
    )
}




export default ShoesList
