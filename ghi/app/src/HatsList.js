import React, { useEffect, useState } from "react";
// import { Link } from 'react-router-dom';


function HatCard(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                console.log(data);
                return (
                <div key={hat.id} className="card mb-5 shadow rounded me-4">
                    <div className="d-flex justify-content-evenly card-title text-center mt-3">
                        <h4 className="card-text mb-2">{hat.fabric}: {hat.style_name}</h4>
                    </div>
                    <img src={hat.image || 'https://via.placeholder.com/300'} className="card-img-top" />
                    <div className="card">
                        <ul className="card-list list-group-flush">
                            <li className="list-group-item" style={{paddingRight: '60px'}}>{hat.color}</li>
                            <li className="list-group-item" style={{paddingRight: '50px'}}>Location Number {hat.location.location_number}</li>
                        </ul>
                    </div>
                    <div className="card-body">
                        <button type="button" className="btn btn-outline-danger " onClick={() => props.onDelete(hat.id)}> Delete</button>
                    </div>

                </div>
                );
            })}
        </div>
    );

}

console.log("start hat list")
function HatsList(props) {
    const [hatCards, setHatCards] = useState([[], [], []]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/locations/hats";

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const columns = [[], [], []];
                let i = 0;
                for (let hat of data.hats) {
                    columns[i].push(hat)
                    i = i + 1;
                    if (i > 2) {
                        i = 0;
                    }
                }
                setHatCards(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

    const onDelete = async (hatID) => {
        const response = await fetch(`http://localhost:8090/api/locations/hats/${hatID}/`,{
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (response.status >= 200 && response.status < 300) {
            fetchData();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="py-5 text-center">
            <h2 className="display-5 fw-bold mb-5">Your Hat Collection</h2>
            <div className="row">
                {hatCards.map((hatList, index) => {
                    return (
                        <HatCard key={index} list={hatList} onDelete={onDelete}/>
                    );
                })}
            </div>
        </div>
    )

}
export default HatsList;
