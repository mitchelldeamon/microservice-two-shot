import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import Nav from './Nav';
function App(props) {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="hats">
                        <Route path="list"  element={<HatsList  hats={props.hats}/>}  />
                        <Route path="new" element={<HatsForm />}  />
                    </Route>
                    <Route path="shoes">
                        <Route path="list" element={<ShoesList shoes={props.shoes}/>} />
                        <Route path="new" element={<ShoesForm />}   />
                    </Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}
export default App;
