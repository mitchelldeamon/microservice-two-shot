# Wardrobify

Team:

- Person 1 - Which microservice?
  Clarisse Alvarez - Hats API, Hats Poller
- Person 2 - Which microservice?
  Mitch Deamon - Shoes API, Shoes Poller

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The Shoes microservice manages shoes and shoe bins with the wardrobe by utilizing the ShoeModel and BinVO models. These models work with the Wardrobe MS to perform operations such as creating(PUSH), retrieving(GET), updating(PUT), and deleting(DELETE) shoes and shoe bins.

## Hats microservice

The Hats microservice stores and retrieves information about hats and hat locations using the HatModel and LocationVO models. The defined models are integrated with the wardrobe API and retrieve information about hats and locations.
