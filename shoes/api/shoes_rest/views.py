from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import ShoeModel, BinVO



# Create your views here.
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "import_href",
        "bin_size",
    ]


class ShoesListEncoder(ModelEncoder):
    model = ShoeModel
    properties = ["model_name", "picture", "manufacturer", "color", "bin", "id"]

    encoders = {
        "bin": BinVOEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = ShoeModel.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400,
            )

        shoe = ShoeModel.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_shoes(request, pk):
    if request.method == "DELETE":
        count, _ = ShoeModel.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        pass